import 'package:flutter/material.dart';

final appTheme = ThemeData(
    primaryColor: Colors.black,
    backgroundColor: Colors.grey.shade200,
    textTheme: TextTheme(
        headline1: TextStyle(
      fontFamily: 'Roboto',
      fontWeight: FontWeight.w100,
      fontSize: 18,
      color: Colors.black,
    )));
