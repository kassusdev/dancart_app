import 'dart:ffi';

import 'package:flutter/material.dart';

// create login model
class LoginModel {
  static List<String> loginForm = [
    '1',
    // ignore: no_adjacent_strings_in_list
    'ahon'
        'kass',
    'test@gmail.com',
    '0642454545',
    '******',
    '******',
  ];
}

@immutable
class Item {
  final int id;
  final String lastname;
  final String firstname;
  final String email;
  final Double phone;
  final String password;
  final String confirmPassword;

  // ignore: sort_constructors_first
  const Item(this.id, this.lastname, this.firstname, this.email, this.phone,
      this.password, this.confirmPassword);

  @override
  int get hashCode => id;

  @override
  bool operator ==(Object other) => other is Item && other.id == id;
}
